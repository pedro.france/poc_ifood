>## **Configuração de ambiente**
___

>###### **1- Ferramentas**

-Android studio com a instalação default. Quando finalizada a instalação,
deverá baixar um emulador (Nexus 5 ou Nexus 5.4).

-Visual Studio Code(recomendavel) ou outro preferivel.

-Appium desktop.

-Cmder.

>###### **2- Variaveis de ambiente**

Comando Abd: realizar o download do android sdk tools (https://developer.android.com/studio/intro/update?hl=pt-br).

setar as variaveis de ambiente da seguinte forma:

###### Variaveis local:

* ANDROID_HOME
* C:\Users\Pedro Santos\AppData\Local\Android\android-sdk

* JAVA_HOME
* C:\Program Files\Java\jdk1.8.0_221

###### Variaveis do sistema:

* ANDROID_HOME
* C:\Users\Pedro Santos\AppData\Local\Android\android-sdk

* JAVA_HOME
* C:\Program Files\Java\jdk1.8.0_221

Path - clique duplo para editar
* %JAVA_HOME%\bin
* C:\Program Files (x86)\Java\jre1.8.0_191\bin
* C:\Ruby25-x64\bin
* %ANDROID_HOME%\bin
* %ANDROID_HOME%\tools
* %ANDROID_HOME%\tools\bin
* %ANDROID_HOME%\platform-tools

APK dentro da pasta support



>###### **3- Usando Report Padrao do Cucumber**

execute o comando: cucumber --format html --out=log/features.html




