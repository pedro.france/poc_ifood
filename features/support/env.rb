require "appium_lib"
require "cucumber"

def caps
{ caps: {  
        deviceName: "emulator-5554",
        platformName: "Android",
        app: (File.join(File.dirname(__FILE__),"test_ifood.apk")),
        appPackage: "com.fooddelivery_pro",
        appActivity: "com.fooddelivery_pro.MainActivity",
        newCommandTimeout: 2000,
        autoWebviewTimeout: 4000,
        noReset: true
    }
}
end

Appium::Driver.new(caps, true)
Appium.promote_appium_methods Object
