#language:pt

Funcionalidade: validar funcionamento do app
    Como futuro cliente do app ifood
    Quero poder cadastrar meus dados no aplicativo
    Para que possa utilizar o mesmo para efetuar compras de alimentos

    Cenario: Cadastro de novo usuario com sucesso
        Dado que deve acessar a tela home do aplicativo food
        Quando preencher todos os campos para primeiro acesso com "Kristin.evans@gmail.com","1234567890" e "12345678"
        E preencher a validacao de autenticação com o token
        Entao deve validar a tela Categories e Popular